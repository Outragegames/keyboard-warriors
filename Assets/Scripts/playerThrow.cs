﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;


public class playerThrow : MonoBehaviour
{
    public PhotonView photonView;
    public GameObject nade;
    public GameObject molotov, frag, smoke, flash, molotovIndicator;
    public Toggle[] damageNade, secNade;
    public Transform throwPoint, weapon;
    public float speed = 4f;
    private float mouseDistance;
    private Vector3 mousePos;
    public int dmgWhatToThrow, secWhatToThrow, firstNum, secondNum;
    public bool hasDmg = true, hasSec = false;

    void Awake()
    {
        chooseNade();
    }

    public void chooseNade()
    {
        if (photonView.IsMine)
        {
            firstNum = 2;
            secondNum = 2;
            damageNade[0] = GameObject.Find("chooseFrag").GetComponent<Toggle>();
            damageNade[1] = GameObject.Find("chooseMolly").GetComponent<Toggle>();
            secNade[0] = GameObject.Find("chooseSmoke").GetComponent<Toggle>();
            secNade[1] = GameObject.Find("chooseFlash").GetComponent<Toggle>();

            if (damageNade[0].isOn)
            {
                dmgWhatToThrow = 3;
            }
            else if (damageNade[1].isOn)
            {
                dmgWhatToThrow = 0;
            }
            if (secNade[0].isOn)
            {
                secWhatToThrow = 1;
            }
            else if (secNade[1].isOn)
            {
                secWhatToThrow = 2;
            }
        }
    }

    void Update()
    {
        if (!GetComponent<playerVariables>().isDead)
        {
            if (photonView.IsMine)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    hasDmg = true;
                    hasSec = false;
                }
                else if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    hasDmg = false;
                    hasSec = true;
                }

                if (Input.GetMouseButton(1))
                {
                    if (hasDmg && firstNum > 0)
                    {
                        SpawnIndicator(0);
                    }
                    else if (hasSec && secondNum > 0)
                    {
                        SpawnIndicator(0);
                    }
                }
                if (Input.GetMouseButtonUp(1))
                {
                    if (hasDmg)
                    {
                        photonView.RPC(nameof(SpawnNade), RpcTarget.AllViaServer, dmgWhatToThrow, mouseDistance, GetComponent<playerHealth>().whosBullet);
                    }
                    else if (hasSec)
                    {
                        photonView.RPC(nameof(SpawnNade), RpcTarget.AllViaServer, secWhatToThrow, mouseDistance, GetComponent<playerHealth>().whosBullet);
                    }
                }

                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mouseDistance = Vector2.Distance(mousePos, transform.position);
                if (mouseDistance > 15)
                {
                    mouseDistance = 15;
                }
            }
        }
    }

    [PunRPC]
    void SpawnNade(int throwable, float mouseDistancee, int whosThrowable)
    {
        switch (throwable)
        {
            case 0:
                if (firstNum > 0)
                {
                    nade = Instantiate(molotov, new Vector2(throwPoint.position.x, throwPoint.position.y), Quaternion.Euler(0f, 0f, (weapon.eulerAngles.z / 2) + 180));
                    nade.GetComponent<throwables>().speed += mouseDistancee * 3;
                    firstNum -= 1;
                }
                break;
            case 1:
                if (secondNum > 0)
                {
                    nade = Instantiate(smoke, new Vector2(throwPoint.position.x, throwPoint.position.y), Quaternion.Euler(0f, 0f, (weapon.eulerAngles.z / 2) + 180));
                    nade.GetComponent<throwables>().speed += mouseDistancee * 3;
                    secondNum -= 1;
                }
                break;
            case 2:
                if (secondNum > 0)
                {
                    nade = Instantiate(flash, new Vector2(throwPoint.position.x, throwPoint.position.y), Quaternion.Euler(0f, 0f, (weapon.eulerAngles.z / 2) + 180));
                    nade.GetComponent<throwables>().speed += mouseDistancee * 3;
                    secondNum -= 1;
                }
                break;
            case 3:
                if (firstNum > 0)
                {
                    nade = Instantiate(frag, new Vector2(throwPoint.position.x, throwPoint.position.y), Quaternion.Euler(0f, 0f, (weapon.eulerAngles.z / 2) + 180));
                    nade.GetComponent<throwables>().speed += mouseDistancee * 3;
                    firstNum -= 1;
                }
                break;
        }

        if (nade != null)
        {
            nade.transform.GetComponent<throwables>().whosThrowable = whosThrowable;
        }
    }

    void SpawnIndicator(int throwable)
    {
        switch (throwable)
        {
            case 0:
                nade = Instantiate(molotovIndicator, new Vector2(throwPoint.position.x, throwPoint.position.y), Quaternion.Euler(0f, 0f, (weapon.eulerAngles.z / 2) + 180));
                nade.GetComponent<throwables>().speed += mouseDistance * 3;
                break;
        }
    }
}
