﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using Cinemachine;
using Keyboard.Warriors;

public class playerHealth : MonoBehaviourPunCallbacks, IPunObservable
{
    public int health = 100;

    public int whosBullet;

    public CinemachineVirtualCamera cameraObj;
    public GameObject[] enemies;
    public int enemyWatchIndex;
    public Animator blood;

    public GameObject hpBar;
    public Color redColor, greenColor;
    public GameObject manager, chooseThings;
    public Transform spawnPoint;
    public string spawnName;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(health);
        }
        else if(stream.IsReading)
        {
            health = (int)stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void TakeDamage(int dmg)
    {
        health -= dmg;
    }

    [PunRPC]
    public void IncreaseXp(int xp)
    {
        GetComponent<playerVariables>().currentXp += xp;
    }

    public void TakeDamage2(int dmg)
    {
        if (photonView.IsMine)
        {
            if (GetComponent<playerUlti>().turdmoon != playerUlti.myTurdmoon.ereboss)
            {
                photonView.RPC(nameof(TakeDamage), RpcTarget.All, dmg);
                blood.SetTrigger("show");
            }
            else
            {
                if(!GetComponent<playerVariables>().ultiActive)
                {
                    photonView.RPC(nameof(TakeDamage), RpcTarget.All, dmg);
                    blood.SetTrigger("show");
                }
            }
        }
    }

    public void Respawn()
    {
        cameraObj.m_Lens.OrthographicSize = 11;
        if (chooseThings.activeSelf)
        {
            GameObject[] newobj = GameObject.FindGameObjectsWithTag("weapon");
            for (int i = 0; i < newobj.Length; i++)
            {
                if (newobj[i].GetComponent<PhotonView>().IsMine)
                {
                    newobj[i].GetComponent<weapon>().chooseWeapon();
                }
            }
            GetComponent<playerThrow>().chooseNade();
            chooseThings.SetActive(false);
        }
        else
        {
            chooseThings.SetActive(true);

            GameObject[] newobj = GameObject.FindGameObjectsWithTag("weapon");
            for (int i = 0; i < newobj.Length; i++)
            {
                if (newobj[i].GetComponent<PhotonView>().IsMine)
                {
                    newobj[i].GetComponent<weapon>().chooseWeapon();
                }
            }
            GetComponent<playerThrow>().chooseNade();

            chooseThings.SetActive(false);
        }
        
        GetComponent<playerThrow>().firstNum = 2;
        GetComponent<playerThrow>().secondNum = 2;
        health = 100;
        GetComponent<playerVariables>().isDead = false;
        health = 100;
        transform.position = new Vector2(spawnPoint.position.x, spawnPoint.position.y);
        cameraObj.m_Follow = gameObject.transform;
    }

    private void Start()
    {
        chooseThings = GameObject.Find("NetworkManager").GetComponent<photonManager>().chooseThings;
        cameraObj = GameObject.Find("CMvcam").GetComponent<CinemachineVirtualCamera>();
        spawnPoint = GameObject.Find(spawnName).transform;
        blood = GameObject.Find("blood").GetComponent<Animator>();
        manager = GameObject.FindGameObjectWithTag("manager");

        if (photonView.IsMine)
        {
            hpBar.GetComponent<SpriteRenderer>().color = greenColor;
            tag = "Player";
            gameObject.layer = 9;
        }
        else
        {
            hpBar.GetComponent<SpriteRenderer>().color = redColor;
            tag = "Enemy";
            gameObject.layer = 10;
        }

            
    }

    [PunRPC]
    public void MakeTagDead()
    {
        tag = "Dead";
        GetComponent<playerVariables>().isDead = true;
    }

    private void Update()
    {
        if (GetComponent<playerVariables>().isDead && Input.GetKeyDown(KeyCode.B))
        {
            if (chooseThings.activeSelf)
            {
                chooseThings.SetActive(false);
            }
            else
            {
                chooseThings.SetActive(true);
            }
        }

        hpBar.transform.localScale = new Vector2((float)health/100, hpBar.transform.localScale.y);
        if (photonView.IsMine)
        {
            if(!GetComponent<playerVariables>().isDead)
            {
                cameraObj.m_Follow = gameObject.transform;
            }
            if(Input.GetKeyDown(KeyCode.Space))
            {
                if (enemyWatchIndex < enemies.Length - 1)
                {
                    enemyWatchIndex += 1;
                }
                else
                {
                    enemyWatchIndex = 0;
                }
                if (enemies != null && GetComponent<playerVariables>().isDead)
                {
                    cameraObj.m_Follow = enemies[enemyWatchIndex].transform;
                }
            }

            enemies = GameObject.FindGameObjectsWithTag("Enemy");

            if (health <= 0 && !GetComponent<playerVariables>().isDead)
            {
                cameraObj = GameObject.Find("CMvcam").GetComponent<CinemachineVirtualCamera>();
                cameraObj.m_Lens.OrthographicSize = 18;
                if (enemies.Length != 0)
                {
                    cameraObj.m_Follow = enemies[enemyWatchIndex].transform;
                }
                transform.position = new Vector2(3000f, 0f);
                photonView.RPC(nameof(MakeTagDead), RpcTarget.All);
            }
        }
            
    }
}