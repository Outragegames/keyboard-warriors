﻿using Cinemachine;
using Photon.Pun;
using Smooth;
using UnityEngine;
using UnityEngine.UI;

namespace Keyboard.Warriors
{
    public class weapon : MonoBehaviour
    {
        public enum myWeapon
        {
            Ak, Sniper, Shotgun, Glock
        }
        public myWeapon myweapon;
        private Rigidbody2D playerRb;
        public GameObject player;
        public Toggle[] gunChoose;

        //animation
        public string currentState, weaponIdle, weaponShoot, weaponReload;
        public Animator anim;
        private bool isShooting = false, isReloading = false, magSpawned;
        public GameObject[] mags;
        public Transform akMagpoint, karMagpoint, shotgunMagpoint, glockMagpoint;

        [SerializeField]
        public GameObject akBullet, sniperBullet, shotgunBullet, glockBullet, playerSprite, wallParts, blood, bullet;
        public Transform shotPoint, goTo;
        private float timeBshots, rotZ;
        public float startTimeBshots, speed, spread = 0;
        public int bullets;
        private Vector3 difference;
        public PhotonView photonView;
        public CinemachineVirtualCamera cameraObj;
        public SpriteRenderer weaponSprite;
        public Sprite[] gunImage;
        public Vector2[] gotoPos;

        private void Awake()
        {
            chooseWeapon();
        }

        public void chooseWeapon()
        {
            if (photonView.IsMine)
            {
                gunChoose[0] = GameObject.Find("chooseAk").GetComponent<Toggle>();
                gunChoose[1] = GameObject.Find("chooseSniper").GetComponent<Toggle>();
                gunChoose[2] = GameObject.Find("chooseShotgun").GetComponent<Toggle>();
                gunChoose[3] = GameObject.Find("chooseGlock").GetComponent<Toggle>();

                if (gunChoose[0].isOn)
                {
                    myweapon = myWeapon.Ak;
                }
                else if (gunChoose[1].isOn)
                {
                    myweapon = myWeapon.Sniper;
                }
                else if (gunChoose[2].isOn)
                {
                    myweapon = myWeapon.Shotgun;
                }
                else if (gunChoose[3].isOn)
                {
                    myweapon = myWeapon.Glock;
                }

                playerRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
                cameraObj = GameObject.Find("CMvcam").GetComponent<CinemachineVirtualCamera>();
                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
                cameraObj.m_Lens.OrthographicSize = 11;

                if (myweapon == myWeapon.Ak)
                {
                    bullets = 30;
                    shotPoint.localPosition = gotoPos[0];
                    ChangeSprite(0);
                    photonView.RPC(nameof(ChangeAnimations), RpcTarget.All, "akIdle", "akShoot", "akReload");
                }
                else if (myweapon == myWeapon.Sniper)
                {
                    bullets = 1;
                    shotPoint.localPosition = gotoPos[1];
                    ChangeSprite(1);
                    photonView.RPC(nameof(ChangeAnimations), RpcTarget.All, "karIdle", "karShoot", "karReload");
                }
                else if (myweapon == myWeapon.Shotgun)
                {
                    bullets = 2;
                    shotPoint.localPosition = gotoPos[2];
                    ChangeSprite(2);
                    photonView.RPC(nameof(ChangeAnimations), RpcTarget.All, "shotgunIdle", "shotgunShoot", "shotgunReload");
                }
                else if (myweapon == myWeapon.Glock)
                {
                    bullets = 80;
                    shotPoint.localPosition = gotoPos[3];
                    ChangeSprite(3);
                    photonView.RPC(nameof(ChangeAnimations), RpcTarget.All, "glockIdle", "glockShoot", "glockReload");
                }
            }
        }

        void Start()
        {
            transform.parent = null;
        }

        [PunRPC]
        void PlayAnim(string newState)
        {
            if (currentState == newState) return;
            anim.Play(newState);
            currentState = newState;
        }

        [PunRPC]
        void ChangeAnimations(string weaponidle, string weaponshoot, string weaponreload)
        {
            weaponIdle = weaponidle;
            weaponShoot = weaponshoot;
            weaponReload = weaponreload;
        }

        void ChangeSprite(int spriteIndex)
        {
            weaponSprite.sprite = gunImage[spriteIndex];
        }

        void Update()
        {
            if (photonView.IsMine)
            {
                if (!isShooting && !isReloading)
                {
                    photonView.RPC(nameof(PlayAnim), RpcTarget.All, weaponIdle);
                }
                else if (isReloading)
                {
                    photonView.RPC(nameof(PlayAnim), RpcTarget.All, weaponReload);
                }
                else if (isShooting)
                {
                    photonView.RPC(nameof(PlayAnim), RpcTarget.All, weaponShoot);
                }
                if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponShoot) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                {
                    if (bullets <= 0)
                    {
                        isShooting = false;
                        isReloading = true;
                    }
                    else
                    {
                        isShooting = false;
                        isReloading = false;
                    }
                }
            }

            if (photonView.IsMine)
            {
                transform.position = goTo.transform.position;
                Vector3 mousePos = Input.mousePosition;
                mousePos.z = 0;
                Vector3 screenPos = Camera.main.ScreenToWorldPoint(mousePos);

                Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
                rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

                if (screenPos.x < gameObject.transform.localPosition.x)
                {
                    transform.rotation = Quaternion.Euler(0f, 0f, rotZ);
                    transform.localScale = new Vector2(transform.localScale.x, -1f);
                    photonView.RPC(nameof(FlipNow), RpcTarget.All, true);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0f, 0f, rotZ);
                    transform.localScale = new Vector2(transform.localScale.x, 1f);
                    photonView.RPC(nameof(FlipNow), RpcTarget.All, false);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
                    spread = 0;
                }

                if (!player.GetComponent<playerVariables>().isDead)
                {
                    if (myweapon == myWeapon.Ak)
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                        {
                            isReloading = false;
                            bullets = 30;
                            magSpawned = false;
                        }
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f
                            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f && !magSpawned)
                        {
                            photonView.RPC(nameof(createMag), RpcTarget.All, akMagpoint.position.x, akMagpoint.position.y, 0);
                            magSpawned = true;
                        }

                        startTimeBshots = 0.05f;
                        if (timeBshots <= 0 && bullets > 0)
                        {
                            if (Input.GetMouseButton(0))
                            {
                                bullets -= 1;
                                isShooting = true;
                                if (spread < 5f)
                                {
                                    spread += 0.2f;
                                }
                                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                                Vector2 ksie = new Vector2(shotPoint.position.x, shotPoint.position.y);

                                float posX = shotPoint.position.x;
                                float posY = shotPoint.position.y;
                                float rotZa = transform.eulerAngles.z;
                                float spreadRange = Random.Range(-spread, spread);

                                if (player.GetComponent<playerVariables>().ultiActive && player.GetComponent<playerUlti>().turdmoon == playerUlti.myTurdmoon.outrage)
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 0, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 2);
                                }
                                else
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 0, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 1);
                                }

                                timeBshots = startTimeBshots;
                                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 3;
                                Invoke(nameof(StopShake), 0.04f);
                            }
                        }
                        else
                        {
                            timeBshots -= Time.deltaTime;
                            StopShake();
                        }
                    }
                    else if (myweapon == myWeapon.Sniper)
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                        {
                            isReloading = false;
                            bullets = 1;
                            magSpawned = false;
                        }
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.6f
                            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.7f && !magSpawned)
                        {
                            photonView.RPC(nameof(createMag), RpcTarget.All, karMagpoint.position.x, karMagpoint.position.y, 1);
                            magSpawned = true;
                        }

                        startTimeBshots = 1f;
                        if (timeBshots <= 0 && bullets > 0)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                bullets -= 1;
                                isShooting = true;
                                playerRb.velocity = new Vector2(0f, 0f);
                                playerRb.velocity = -transform.right * 30f;
                                float posX = shotPoint.position.x;
                                float posY = shotPoint.position.y;
                                float rotZa = transform.eulerAngles.z;

                                if (player.GetComponent<playerVariables>().ultiActive && player.GetComponent<playerUlti>().turdmoon == playerUlti.myTurdmoon.outrage)
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, 0f, 1, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 2);
                                }
                                else
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, 0f, 1, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 1);
                                }

                                timeBshots = startTimeBshots;
                                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 50;
                                Invoke(nameof(StopShake), 0.1f);
                            }
                        }
                        else
                        {
                            timeBshots -= Time.deltaTime;
                        }
                    }
                    else if (myweapon == myWeapon.Shotgun)
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                        {
                            isReloading = false;
                            bullets = 2;
                            magSpawned = false;
                        }
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f
                            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f && !magSpawned)
                        {
                            photonView.RPC(nameof(createMag), RpcTarget.All, shotgunMagpoint.position.x, shotgunMagpoint.position.y, 2);
                            magSpawned = true;
                        }

                        startTimeBshots = 0.4f;
                        if (timeBshots <= 0 && bullets > 0)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                bullets -= 1;
                                isShooting = true;
                                playerRb.velocity = new Vector2(0f, 0f);
                                playerRb.velocity = -transform.right * 30f;

                                for (int i = 0; i < 8; i++)
                                {
                                    float posX = shotPoint.position.x;
                                    float posY = shotPoint.position.y;
                                    float rotZa = transform.eulerAngles.z;
                                    float spreadRange = Random.Range(-8f, 8f);

                                    if (player.GetComponent<playerVariables>().ultiActive && player.GetComponent<playerUlti>().turdmoon == playerUlti.myTurdmoon.outrage)
                                    {
                                        photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 2, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 2);
                                    }
                                    else
                                    {
                                        photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 2, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 1);
                                    }
                                }
                                timeBshots = startTimeBshots;

                                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 15;
                                Invoke(nameof(StopShake), 0.1f);
                            }
                        }
                        else
                        {
                            timeBshots -= Time.deltaTime;
                        }
                    }
                    else if (myweapon == myWeapon.Glock)
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                        {
                            isReloading = false;
                            bullets = 80;
                            magSpawned = false;
                        }
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName(weaponReload) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f
                            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f && !magSpawned)
                        {
                            photonView.RPC(nameof(createMag), RpcTarget.All, glockMagpoint.position.x, glockMagpoint.position.y, 3);
                            magSpawned = true;
                        }

                        startTimeBshots = 0.01f;
                        if (timeBshots <= 0 && bullets > 0)
                        {
                            if (Input.GetMouseButton(0))
                            {
                                bullets -= 1;
                                isShooting = true;
                                if (spread < 2f)
                                {
                                    spread += 0.08f;
                                }
                                float posX = shotPoint.position.x;
                                float posY = shotPoint.position.y;
                                float rotZa = transform.eulerAngles.z;
                                float spreadRange = Random.Range(-spread, spread);

                                if (player.GetComponent<playerVariables>().ultiActive && player.GetComponent<playerUlti>().turdmoon == playerUlti.myTurdmoon.outrage)
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 3, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 2);
                                }
                                else
                                {
                                    photonView.RPC(nameof(spawnBullet), RpcTarget.AllViaServer, spreadRange, 3, player.GetComponent<playerHealth>().whosBullet, posX, posY, rotZa, 1);
                                }
                                timeBshots = startTimeBshots;

                                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 2;
                                Invoke(nameof(StopShake), 0.008f);
                            }
                        }
                        else
                        {
                            timeBshots -= Time.deltaTime;
                        }
                    }
                }
            }
        }
        void StopShake()
        {
            cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
        }

        //public static bool validateStateOfPlayer(StatePUN2 latestReceivedState, StatePUN2 latestValidatedState)
        //{
        //    if (Vector3.Distance(latestReceivedState.position, latestValidatedState.position) > 9000.0f &&
        //        (latestReceivedState.ownerTimestamp - latestValidatedState.receivedOnServerTimestamp < .5f))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        [PunRPC]
        void FlipNow(bool flipbool)
        {
            playerSprite.GetComponent<SpriteRenderer>().flipX = flipbool;
        }

        [PunRPC]
        void createMag(float spawnPointX, float spawnPointY, int bulletType)
        {
            GameObject mag;
            switch (bulletType)
            {
                case 0:
                    mag = Instantiate(mags[0], new Vector2(spawnPointX, spawnPointY), transform.rotation);
                    break;
                case 1:
                    mag = Instantiate(mags[2], new Vector2(spawnPointX, spawnPointY), transform.rotation);
                    break;
                case 2:
                    mag = Instantiate(mags[2], new Vector2(spawnPointX, spawnPointY), transform.rotation);
                    break;
                case 3:
                    mag = Instantiate(mags[1], new Vector2(spawnPointX, spawnPointY), transform.rotation);
                    break;
            }
        }

        [PunRPC]
        void spawnBullet(float spreadd, int bulletType, int whosBullet, float posX, float posY, float rotZ, int multiplier)
        {
            switch (bulletType)
            {
                case 0:
                    bullet = Instantiate(akBullet, new Vector2(posX, posY),
                        Quaternion.Euler(0f, 0f, (rotZ / 2) + 180 + spreadd));
                    break;
                case 1:
                    bullet = Instantiate(sniperBullet, new Vector2(posX, posY),
                        Quaternion.Euler(0f, 0f, (rotZ / 2) + 180 + spreadd));
                    break;
                case 2:
                    bullet = Instantiate(shotgunBullet, new Vector2(posX, posY),
                        Quaternion.Euler(0f, 0f, (rotZ / 2) + 180 + spreadd));
                    break;
                case 3:
                    bullet = Instantiate(glockBullet, new Vector2(posX, posY),
                        Quaternion.Euler(0f, 0f, (rotZ / 2) + 180 + spreadd));
                    break;
            }

            
            bullet.GetComponent<bulletMovement>().damage *= multiplier;
            
            bullet.transform.GetComponent<bulletMovement>().whosBullet = whosBullet;

            if (!photonView.IsMine)
            {
                bullet.tag = "enemyBullet";
            }
            else
            {
                bullet.tag = "Bullet";
            }

            if (photonView.IsMine)
            {
                bullet.layer = 11;
            }
            else
            {
                bullet.layer = 14;
            }
        }
    }

}