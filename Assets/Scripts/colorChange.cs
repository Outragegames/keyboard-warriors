﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class colorChange : MonoBehaviour
{
    float timeLeft;
    Color targetColor;

    void Start()
    {
        GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
        targetColor = new Color(0f, 0f, 0f, 1f);
    }

    void Update()
    {
        if (timeLeft <= Time.deltaTime)
        {
            GetComponent<Image>().color = targetColor;
            targetColor = Color.HSVToRGB(Random.value, 0.7f, 1f);
            timeLeft = 1.0f;
        }
        else
        {
            GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, targetColor, Time.deltaTime / timeLeft);
            timeLeft -= Time.deltaTime;
        }
    }
}
