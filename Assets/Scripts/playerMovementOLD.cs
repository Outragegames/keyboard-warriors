﻿using UnityEngine;
using Photon.Pun;
using Smooth;


    public class playerMovementOLD : MonoBehaviour
    {
        [SerializeField]
        float walkSpeed, jumpSpeed;
        public Rigidbody2D rb;
        public PhotonView photonView;
        public bool isGrounded;
        public LayerMask groundMask;
        public float groundCheckY, groundCheckSizeX, groundCheckSizeY;
        private playerVariables playerVars;
        private float myInput;
        public bool canJump;
        public float jumpTime = 0.1f;
        public PhysicsMaterial2D nofricMat, fricMat;

        void Start()
        {
            playerVars = GetComponent<playerVariables>();
        }

        public static bool validateStateOfPlayer(StatePUN2 latestReceivedState, StatePUN2 latestValidatedState)
        {
            if (Vector3.Distance(latestReceivedState.position, latestValidatedState.position) > 9000.0f &&
                (latestReceivedState.ownerTimestamp - latestValidatedState.receivedOnServerTimestamp < .5f))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.name == "elevator")
            {
                rb.velocity = new Vector2(rb.velocity.x, collision.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            }
        }

        void Update()
        {
            if (photonView.IsMine)
            {
                if(GetComponent<playerUlti>().turdmoon == playerUlti.myTurdmoon.loker && GetComponent<playerVariables>().ultiActive)
                {
                    walkSpeed = 700 * 2f;
                    jumpSpeed = 47 * 1.5f;
                }
                else
                {
                    walkSpeed = 700;
                    jumpSpeed = 47;
                }
                playerVars.isJumping = !isGrounded;

                myInput = Input.GetAxisRaw("Horizontal");
                rb.velocity = new Vector2(myInput * walkSpeed * Time.fixedDeltaTime, rb.velocity.y);

                isGrounded = Physics2D.OverlapBox(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - groundCheckY),
                new Vector2(groundCheckSizeX, groundCheckSizeY), 0f, groundMask);

                if (Input.GetKeyDown(KeyCode.Space) && canJump)
                {
                    rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
                    isGrounded = false;
                    playerVars.isJumping = true;
                    jumpTime = -5;
                }

                if (isGrounded)
                {
                    jumpTime = 0.1f;
                }
                else
                {
                    jumpTime -= Time.deltaTime;
                }

                if (jumpTime <= 0)
                {
                    canJump = false;
                }
                else
                {
                    canJump = true;
                }

                if (myInput != 0)
                {
                    playerVars.isWalking = true;
                }
                else
                {
                    playerVars.isWalking = false;
                }  
                
                if(playerVars.isJumping || playerVars.isWalking)
                {
                    rb.sharedMaterial = nofricMat;
                }
                else
                {
                    rb.sharedMaterial = fricMat;
                }
            }            
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - groundCheckY), new Vector2(groundCheckSizeX, groundCheckSizeY));
        }
    }