﻿using UnityEngine;
using System.Collections;
using Smooth;
using System.Linq;
using Photon.Pun;


public class bulletMovement : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public int damage;
    private GameObject player;
    public Camera mainCam;
    public GameObject explosion, blood;
    private Rigidbody2D rb;
    public int bulletType, whosBullet;
    public float reduceSpeedRate;

    void Start()
    {
        Destroy(gameObject, lifeTime);
        player = GameObject.FindGameObjectWithTag("Player");
        mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        rb = GetComponent<Rigidbody2D>();
        if (bulletType == 2)
        {
            speed += Random.Range(-20,20);
        }
    }

    private void Update()
    {
        if (bulletType == 2)
        {
            speed -= reduceSpeedRate * Time.deltaTime;
            if(speed <= 0)
            {
                speed = 0;
            }
        }
        Movement();
    }

    public void DestroyObject()
    {
        Destroy(this.gameObject);
    }

    public void Movement()
    {
        Vector3 localVelocity = Vector3.ClampMagnitude(transform.right, 1) * speed;
        rb.velocity = transform.TransformDirection(localVelocity);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            if (whosBullet != other.gameObject.GetComponent<playerHealth>().whosBullet)
            {
                other.gameObject.GetComponent<playerHealth>().TakeDamage2(damage);
                Instantiate(blood, transform.position, Quaternion.identity);
                Destroy(gameObject);

                GameObject[] tag_1 = GameObject.FindGameObjectsWithTag("Player");
                GameObject[] tag_2 = GameObject.FindGameObjectsWithTag("Enemy");
                GameObject[] final_array = tag_1.Concat(tag_2).ToArray();
                for (int i = 0; i < final_array.Length; i++)
                {
                    if(whosBullet == final_array[i].GetComponent<playerHealth>().whosBullet)
                    {
                        if(other.gameObject.GetComponent<playerUlti>().turdmoon != playerUlti.myTurdmoon.ereboss && !other.gameObject.GetComponent<playerVariables>().ultiActive)
                        {
                            if (final_array[i].GetComponent<PhotonView>().IsMine)
                            {
                                final_array[i].GetComponent<PhotonView>().RPC("IncreaseXp", RpcTarget.All, damage);
                            }
                        }
                    }
                }
            }
            else if (whosBullet == other.gameObject.GetComponent<playerHealth>().whosBullet)
            {
                Physics2D.IgnoreCollision(GetComponent<CircleCollider2D>(), other.transform.GetComponent<CapsuleCollider2D>(), false);
            }
        }
            
        if (other.gameObject.tag == "Ground")
        {
            if (rb != null)
            {
                rb.velocity = new Vector2(0f, 0f);
            }
            speed = 0;

            Instantiate(explosion, transform.position, Quaternion.identity);

            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().sprite = null;
                

            Destroy(gameObject, 2f);
        }
    }
       
}
