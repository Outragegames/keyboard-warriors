﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Photon.Pun;
using Photon.Realtime;

public class chooseTurdmoon : MonoBehaviour, IPointerDownHandler
{
    public Sprite unclicked, clickedForMe, clickedForOthers;
    public Image[] bgs;
    public int thisImage, chosenBy;
    public PhotonView photonView;
    public photonManager photonManager;

    void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    [PunRPC]
    public void ChangeValue(int actornum)
    {
        chosenBy = actornum;
    }

    [PunRPC]
    public void BringToZero(int i)
    {
        bgs[i].gameObject.GetComponentInParent<chooseTurdmoon>().chosenBy = 0;
    }

    private void Update()
    {
        if(chosenBy > 0 && chosenBy == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            bgs[thisImage].sprite = clickedForMe;
            photonManager.spawnIndex = thisImage;
        }
        else if (chosenBy > 0 && chosenBy != PhotonNetwork.LocalPlayer.ActorNumber)
        {
            bgs[thisImage].sprite = clickedForOthers;
        }
        else if (chosenBy == 0)
        {
            bgs[thisImage].sprite = unclicked;
        }

        for (int i = 0; i < bgs.Length; i++)
        {
            if(bgs[i].sprite == clickedForMe)
            {
                photonManager.canBeReady = true;
                break;
            }
            else
            {
                photonManager.canBeReady = false;
            }
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (chosenBy == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            photonView.RPC(nameof(BringToZero), RpcTarget.All, thisImage);
        }
        else if (chosenBy == 0)
        {
            for (int i = 0; i < bgs.Length; i++)
            {
                if (bgs[i].gameObject.GetComponentInParent<chooseTurdmoon>().chosenBy == PhotonNetwork.LocalPlayer.ActorNumber)
                {
                    photonView.RPC(nameof(BringToZero), RpcTarget.All, i);
                }
            }
            photonView.RPC(nameof(ChangeValue), RpcTarget.All, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }
}