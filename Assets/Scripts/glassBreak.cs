﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class glassBreak : MonoBehaviour
{
    public GameObject brokenGlass;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "enemyBullet" || collision.gameObject.layer == 11)
        {
            Instantiate(brokenGlass, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 11)
        {
            Instantiate(brokenGlass, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
