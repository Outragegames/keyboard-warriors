﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Cinemachine;

public class playerNetworking : MonoBehaviour
{
    public MonoBehaviour[] scriptsToIgnore;

    private PhotonView photonView;

    private CinemachineVirtualCamera vcam;

    public GameObject weapon;

    private Rigidbody2D rb;

    public HardLight2D myLight;
    public BoxCollider2D[] cols;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        photonView = GetComponent<PhotonView>();

        if (photonView.IsMine)
        {
            myLight.enabled = true;
            for (int i = 0; i < cols.Length; i++)
            {
                Destroy(cols[i]);
            }
        }
        else
        {
            myLight.enabled = false;
        }
    }
    private void Update()
    {
        if (!photonView.IsMine)
        {
            myLight.enabled = true;
        }
    }

    void Start()
    {
        if (photonView.IsMine)
        {
            vcam = FindObjectOfType<CinemachineVirtualCamera>();
            vcam.m_Follow = gameObject.transform;
        }

        if (!photonView.IsMine)
        {
            foreach (var script in scriptsToIgnore)
            {
                script.enabled = false;
            }
        }

        if (photonView.IsMine)
        {
            gameObject.tag = "Player";
        }
        else
        {
            gameObject.tag = "Enemy";
        }
    }
}
