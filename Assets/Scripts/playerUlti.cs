﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class playerUlti : MonoBehaviour
{
    public enum myTurdmoon
    {
        kosas, ereboss, outrage, juice, loker
    }
    public myTurdmoon turdmoon;
    public PhotonView photonView;
    public GameObject[] ultiParticle, ultiIndicator;
    public GameObject[] sprites;
    public GameObject health, ultiSpawner;

    void Update()
    {
        if (photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.R) && !GetComponent<playerVariables>().ultiActive && GetComponent<playerVariables>().hud[7].GetComponent<Image>().fillAmount == 1
                && !GetComponent<playerVariables>().isDead)
            {
                GetComponent<playerVariables>().hud[7].GetComponent<Image>().fillAmount = 0;
                GetComponent<playerVariables>().ultiActive = true;
                Invoke(nameof(makeInactive), 5f);
                photonView.RPC(nameof(createParticle), RpcTarget.All);
                if (turdmoon == myTurdmoon.kosas && sprites != null)
                {
                    GameObject ultiPart, ultiInd;
                    ultiPart = Instantiate(ultiParticle[0], transform.position, transform.rotation);
                    ultiPart.transform.parent = gameObject.transform;
                    ultiInd = Instantiate(ultiIndicator[0], new Vector2(gameObject.transform.position.x + 0.42f, gameObject.transform.position.y + 2.36f), transform.rotation);
                    ultiInd.transform.parent = gameObject.transform;
                    photonView.RPC(nameof(becomeInvisible), RpcTarget.Others);
                }
                if (turdmoon == myTurdmoon.juice)
                {
                    transform.localScale = new Vector2(0.2f, 0.2f);
                    GetComponent<playerMovementOLD>().groundCheckSizeX = 0.02f;
                }
            }
            if (turdmoon == myTurdmoon.kosas && sprites != null && !GetComponent<playerVariables>().ultiActive)
            {
                photonView.RPC(nameof(becomeVisible), RpcTarget.Others);
            }
        }
    }

    void makeInactive()
    {
        GetComponent<playerVariables>().ultiActive = false;
        transform.localScale = new Vector2(1f, 1f);
        GetComponent<playerMovementOLD>().groundCheckSizeX = 0.64f;
    }

    [PunRPC]
    void becomeVisible()
    {
        if (sprites != null)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                if (sprites[i].GetComponent<SpriteRenderer>())
                {
                    sprites[i].GetComponent<SpriteRenderer>().enabled = true;
                }
                else if (sprites[i].GetComponent<TMP_Text>())
                {
                    sprites[i].GetComponent<TMP_Text>().enabled = true;
                }
            }
        }
        health.SetActive(true);
    }

    [PunRPC]
    void becomeInvisible()
    {
        if (sprites != null)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                if (sprites[i].GetComponent<SpriteRenderer>())
                {
                    sprites[i].GetComponent<SpriteRenderer>().enabled = false;
                }
                else if (sprites[i].GetComponent<TMP_Text>())
                {
                    sprites[i].GetComponent<TMP_Text>().enabled = false;
                }
            }
        }
        health.SetActive(false);
    }

    [PunRPC]
    void createParticle()
    {
        GameObject ultiPart, ultiInd;
        switch (turdmoon)
        {
            case myTurdmoon.ereboss:
                ultiPart = Instantiate(ultiParticle[1], transform.position, transform.rotation);
                ultiPart.transform.parent = gameObject.transform;
                ultiInd = Instantiate(ultiIndicator[1], ultiSpawner.transform.position, transform.rotation);
                ultiInd.transform.parent = gameObject.transform;
                break;
            case myTurdmoon.outrage:
                ultiPart = Instantiate(ultiParticle[2], transform.position, transform.rotation);
                ultiPart.transform.parent = gameObject.transform;
                ultiInd = Instantiate(ultiIndicator[2], ultiSpawner.transform.position, transform.rotation);
                ultiInd.transform.parent = gameObject.transform;
                break;
            case myTurdmoon.loker:
                ultiPart = Instantiate(ultiParticle[4], transform.position, transform.rotation);
                ultiPart.transform.parent = gameObject.transform;
                ultiInd = Instantiate(ultiIndicator[3], ultiSpawner.transform.position, transform.rotation);
                ultiInd.transform.parent = gameObject.transform;
                break;
        }
    }
}