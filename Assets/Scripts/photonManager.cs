﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using System.Linq;
using Cinemachine;
using Boo.Lang;
using UnityEngine.SceneManagement;

public class photonManager : MonoBehaviourPunCallbacks
{
    public GameObject section1, section2, winnerScreen;
    public Transform[] spawnPoints;
    public int spawnIndex, allReady, maxPlayers, roundNum;
    public GameObject[] turdmoon;
    public GameObject desrtoyWhenStart, showPing;
    public bool[] playerReady;
    public bool canBeReady, canwin, winAdded;
    public GameObject chooseThings, inGameUI, tabMenu, escapeMenu, exitBtn;
    public GameObject[] allPlayers, alivePlayers, deadPlayers;
    public GameObject[] levelTexts, winTexts, deadTabs;
    public GameObject[] stackedPlayers, disableChoose;
    public float exitTimer;

    private void Awake()
    {
        chooseThings.SetActive(true);
    }

    private void Start()
    {
        //connectToPhoton();
    }

    public void Disconnectt()
    {
        SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
    }

    void disableWinner()
    {
        winnerScreen.SetActive(false);
    }

    void PressEscape()
    {
        if (escapeMenu.activeSelf)
        {
            escapeMenu.SetActive(false);
        }
        else if (!escapeMenu.activeSelf)
        {
            escapeMenu.SetActive(true);
            exitTimer = 5f;
        }
    }

    void StartAgain()
    {
        roundNum += 1;
        winnerScreen.GetComponentInChildren<TMP_Text>().text = "Round " + roundNum;

        for (int i = 0; i < 5; i++)
        {
            if (stackedPlayers[i] != null)
            {
                deadTabs[i].GetComponent<Image>().color = new Color(255f, 255f, 255f, 0f);
            }
        }

        for (int i = 0; i < allPlayers.Length; i++)
        {
            if (allPlayers[i].GetComponent<PhotonView>().IsMine)
            {
                allPlayers[i].tag = "Player";
            }
            else
            {
                allPlayers[i].tag = "Enemy";
            }
        }

        for (int i = 0; i < allPlayers.Length; i++)
        {
            allPlayers[i].GetComponent<playerHealth>().Respawn();
        }

        canwin = false;
        winAdded = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PressEscape();
        }
        exitTimer -= Time.deltaTime;
        if (exitTimer > 0f)
        {
            exitBtn.GetComponentInChildren<TMP_Text>().text = "You can exit in \n" + (int)exitTimer + " seconds";
            exitBtn.GetComponentInChildren<TMP_Text>().fontSize = 70;
            exitBtn.GetComponent<Button>().interactable = false;
            if(exitTimer < 2f)
            {
                exitBtn.GetComponentInChildren<TMP_Text>().text = "You can exit in \n" + (int)exitTimer + " second";
            }
            if (exitTimer < 1f)
            {
                exitBtn.GetComponentInChildren<TMP_Text>().text = "pls don't";
                exitBtn.GetComponentInChildren<TMP_Text>().fontSize = 150;
            }
        }
        else
        {
            exitBtn.GetComponentInChildren<TMP_Text>().text = "Exit";
            exitBtn.GetComponentInChildren<TMP_Text>().fontSize = 150;
            exitBtn.GetComponent<Button>().interactable = true;
        }

        stackedPlayers[0] = GameObject.Find("kosas(Clone)");
        stackedPlayers[1] = GameObject.Find("Ereboss(Clone)");
        stackedPlayers[2] = GameObject.Find("outrage(Clone)");
        stackedPlayers[3] = GameObject.Find("juice(Clone)");
        stackedPlayers[4] = GameObject.Find("loker(Clone)");

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            tabMenu.SetActive(true);
        }
        if(Input.GetKeyUp(KeyCode.Tab))
        {
            tabMenu.SetActive(false);
        }

        for (int i = 0; i < 5; i++)
        {
            if (stackedPlayers[i] != null)
            {
                levelTexts[i].GetComponent<TMP_Text>().text = stackedPlayers[i].GetComponent<playerVariables>().currentLevel.ToString();
                winTexts[i].GetComponent<TMP_Text>().text = stackedPlayers[i].GetComponent<playerVariables>().wins.ToString();
            }
        }

        for (int i = 0; i < 5; i++)
        {
            if (stackedPlayers[i] != null)
            {
                if (stackedPlayers[i].GetComponent<playerVariables>().isDead)
                {
                    deadTabs[i].GetComponent<Image>().color = new Color(255f, 255f, 255f, 0.9f);
                }
            }
        }

        GameObject[] tag_a = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] tag_b = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] tag_c = GameObject.FindGameObjectsWithTag("Dead");
        allPlayers = tag_a.Concat(tag_b).Concat(tag_c).ToArray();

        GameObject[] tag_1 = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] tag_2 = GameObject.FindGameObjectsWithTag("Enemy");
        alivePlayers = tag_1.Concat(tag_2).ToArray();

        deadPlayers = GameObject.FindGameObjectsWithTag("Dead");

        if (deadPlayers.Length >= maxPlayers - 1 && !canwin && deadPlayers.Length != 0)
        {
            canwin = true;
        }

        if (canwin && !winAdded)
        {
            if (allPlayers[0].gameObject.GetComponent<playerVariables>().wins >= 9)
            {
                winnerScreen.SetActive(true);
                winnerScreen.GetComponentInChildren<TMP_Text>().text = "The winner is: " + allPlayers[0].gameObject.GetComponent<playerUlti>().turdmoon;
                winnerScreen.GetComponentInChildren<Animator>().SetTrigger("fadeWinner");
                allPlayers[0].gameObject.GetComponent<playerVariables>().wins += 1;
                Invoke("Disconnectt", 3f);
            }
            else
            {
                winnerScreen.SetActive(true);
                winnerScreen.GetComponentInChildren<TMP_Text>().text = allPlayers[0].gameObject.GetComponent<playerUlti>().turdmoon + " won";
                winnerScreen.GetComponentInChildren<Animator>().SetTrigger("fadeWinner");
                allPlayers[0].gameObject.GetComponent<playerVariables>().wins += 1;
                Invoke(nameof(StartAgain), 3f);
            }
            winAdded = true;
        }

        maxPlayers = PhotonNetwork.PlayerList.Length;


        if (Input.GetKeyDown(KeyCode.F))
        {
            if (showPing.activeSelf)
            {
                showPing.SetActive(false);
            }
            else
            {
                showPing.SetActive(true);
            }
        }

        if (PhotonNetwork.IsConnected && canBeReady)
        {
            if (!playerReady[PhotonNetwork.LocalPlayer.ActorNumber - 1])
            {
                section1.GetComponentInChildren<TMP_Text>().text = "PLAY";
            }
            else
            {
                section1.GetComponentInChildren<TMP_Text>().text = "Waiting for others...";
            }
        }

        if(!canBeReady)
        {
            section1.GetComponentInChildren<TMP_Text>().text = "PLAY";
            photonView.RPC(nameof(ImReady2), RpcTarget.All, false, PhotonNetwork.LocalPlayer.ActorNumber - 1);
        }

        if (allReady == maxPlayers)
        {
            for (int i = 0; i < disableChoose.Length; i++)
            {
                disableChoose[i].SetActive(false);
            }
            Invoke(nameof(disableThemFun), 0.05f);
            winnerScreen.SetActive(true);
            winnerScreen.GetComponentInChildren<TMP_Text>().text = "Round " + roundNum;
            winnerScreen.GetComponentInChildren<Animator>().SetTrigger("fadeRound");
            PhotonNetwork.Instantiate(turdmoon[spawnIndex].name, spawnPoints[spawnIndex].position, Quaternion.identity);
            allReady = 0;
        }
    }

    void disableThemFun()
    {
        chooseThings.SetActive(false);
    }

    [PunRPC]
    public void makeReady(bool makeTrue)
    {
        if (makeTrue)
        {
            allReady += 1;
        }
        else
        {
            allReady -= 1;
        }
    }

    [PunRPC]
    public void ImReady2(bool makeTrue, int playerNum)
    {
        playerReady[playerNum] = makeTrue;
    }

    public void ImReady()
    {
        if (PhotonNetwork.IsConnected && canBeReady)
        {
            if (!playerReady[PhotonNetwork.LocalPlayer.ActorNumber - 1])
            {
                photonView.RPC(nameof(ImReady2), RpcTarget.All, true, PhotonNetwork.LocalPlayer.ActorNumber - 1);
                photonView.RPC(nameof(makeReady), RpcTarget.All, true);
            }
            else
            {
                photonView.RPC(nameof(ImReady2), RpcTarget.All, false, PhotonNetwork.LocalPlayer.ActorNumber - 1);
                photonView.RPC(nameof(makeReady), RpcTarget.All, false);
            }
        }
    }

    public void connectToPhoton()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        Debug.Log("CONNECTED TO MASTER!");
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinOrCreateRoom("Room", new RoomOptions { MaxPlayers = 5 }, TypedLobby.Default);
        Debug.Log("JOINED!");
    }
}
