﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class throwables : MonoBehaviour
{
    private Rigidbody2D rb;
    public float speed;
    public float upSpeed;
    public GameObject boomParticle;
    public bool isIndicator, isMolotov;
    public int whosThrowable;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Movement();
        Invoke(nameof(DestroyParticle), 1.3f);
    }

    void SpawnParticle()
    {
        GameObject part = Instantiate(boomParticle, transform.position, Quaternion.identity);
        if (part.GetComponent<destroyAfter>())
        {
            part.GetComponent<destroyAfter>().whosThrowable = whosThrowable;
        }
        if (part.GetComponent<mollyFire>())
        {
            part.GetComponent<mollyFire>().whosThrowable = whosThrowable;
        }
    }

    void DestroyParticle()
    {
        if (boomParticle != null)
        {
            SpawnParticle();
        }

        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground" && isIndicator)
        {
            Destroy(gameObject);
        }
        if(isMolotov)
        {
            DestroyParticle();
        }
    }

    public void Movement()
    {
        //transform.Translate(transform.right * speed * Time.deltaTime);

        Vector3 localVelocity = Vector3.ClampMagnitude(transform.right, 1) ;
        rb.velocity = transform.TransformDirection(localVelocity) * speed;
        //rb.velocity = new Vector2(rb.velocity.x, upSpeed);

        
    }
}
