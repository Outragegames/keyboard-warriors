﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevatorMove : MonoBehaviour
{
    public int dir = 1;
    public float speed;

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, dir * speed);
    }

    IEnumerator ChangeDir()
    {
        while(true)
        {
            yield return new WaitForSeconds(5f);
            dir *= -1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "elstopper")
        {
            dir *= -1;
        }
    }
}
