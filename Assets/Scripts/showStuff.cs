﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;

public class showStuff : MonoBehaviour
{
    public enum player
    {
        player1, player2, player3, player4, player5
    };

    public player playerID;

    public TMP_Text pingNum, playerNum;
    void Start()
    {
        
    }

    void Update()
    {
        pingNum.text = PhotonNetwork.GetPing().ToString();
        playerNum.text = PhotonNetwork.LocalPlayer.GetPlayerNumber().ToString();
    }
}
