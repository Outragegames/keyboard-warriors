﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Pun;
using Keyboard.Warriors;

public class playerVariables : MonoBehaviour
{
    public bool isWalking, isJumping, ultiActive, isDead = false;
    public int nextXp, currentXp, currentLevel, wins;
    public GameObject xpBar, levelNum, myUsername;
    public PhotonView photonView;
    public GameObject[] hud;
    public Sprite nade, molly, smoke, flash, normalnade, selectednade1, selectednade2;

    [PunRPC]
    void changeName(string name)
    {
        myUsername.GetComponent<TMP_Text>().text = name;
    }

    void Start()
    {
        photonView = GetComponent<PhotonView>();

        if (photonView.IsMine)
        {
            string name = PhotonNetwork.LocalPlayer.NickName;
            photonView.RPC(nameof(changeName), RpcTarget.All, name);
        }
        

        currentLevel = 1;
        

        if(photonView.IsMine)
        {
            hud[0] = GameObject.Find("playerLevelNumber");
            hud[1] = GameObject.Find("playerHpBar");
            hud[2] = GameObject.Find("playerLevelBar");
            hud[3] = GameObject.Find("playerProjectile1");
            hud[4] = GameObject.Find("playerProjectile2");
            hud[5] = GameObject.Find("playerProjectile1Number");
            hud[6] = GameObject.Find("playerProjectile2Number");
            hud[7] = GameObject.Find("playerUltiBar");
            hud[8] = GameObject.Find("playerProjSelect1");
            hud[9] = GameObject.Find("playerProjSelect2");
        }
    }

    [PunRPC]
    public void changeTag(string tagtoChange)
    {
        tag = tagtoChange;
    }

    [PunRPC]
    public void MakeAliveAgain()
    {
        GetComponent<playerHealth>().health = 100;
        isDead = false;
        GetComponent<playerThrow>().firstNum = 2;
        GetComponent<playerThrow>().secondNum = 2;
    }
    

    void Update()
    {
        if (photonView.IsMine)
        {
            hud[0].GetComponent<TMP_Text>().text = currentLevel.ToString();
            hud[1].GetComponent<Image>().fillAmount = (float)GetComponent<playerHealth>().health / 100;
            hud[2].GetComponent<Image>().fillAmount = (float)currentXp / nextXp;

            if (GetComponent<playerThrow>().dmgWhatToThrow == 0)
            {
                hud[3].GetComponent<Image>().sprite = molly;
            }
            else if (GetComponent<playerThrow>().dmgWhatToThrow == 3)
            {
                hud[3].GetComponent<Image>().sprite = nade;
            }

            if (GetComponent<playerThrow>().secWhatToThrow == 1)
            {
                hud[4].GetComponent<Image>().sprite = smoke;
            }
            else if (GetComponent<playerThrow>().secWhatToThrow == 2)
            {
                hud[4].GetComponent<Image>().sprite = flash;
            }

            hud[5].GetComponent<TMP_Text>().text = "x" + GetComponent<playerThrow>().firstNum.ToString();
            hud[6].GetComponent<TMP_Text>().text = "x" + GetComponent<playerThrow>().secondNum.ToString();

            if (currentLevel < 5)
            {
                hud[7].GetComponent<Image>().fillAmount = (float)nextXp / 150;
            }
            else
            {
                if(hud[7].GetComponent<Image>().fillAmount < 1)
                {
                    hud[7].GetComponent<Image>().fillAmount += Time.deltaTime / 20;
                }
            }

            if (GetComponent<playerThrow>().hasDmg)
            {
                hud[8].GetComponent<Image>().sprite = selectednade1;
                hud[9].GetComponent<Image>().sprite = normalnade;
            }
            else
            {
                hud[8].GetComponent<Image>().sprite = normalnade;
                hud[9].GetComponent<Image>().sprite = selectednade2;
            }

            
            GameObject[] newobj = GameObject.FindGameObjectsWithTag("weapon");
            for (int i = 0; i < newobj.Length; i++)
            {
                if (newobj[i].GetComponent<PhotonView>().IsMine)
                {
                    hud[10].GetComponent<TMP_Text>().text = newobj[i].GetComponent<weapon>().bullets.ToString();
                }
            }
        }


        levelNum.GetComponent<TMP_Text>().text = currentLevel.ToString();
        nextXp = currentLevel * 30;
        xpBar.transform.localScale = new Vector2((float)currentXp / nextXp, xpBar.transform.localScale.y);

        if(currentXp >= nextXp)
        {
            currentXp = currentXp - nextXp;
            currentLevel += 1;
        }
    }
}
