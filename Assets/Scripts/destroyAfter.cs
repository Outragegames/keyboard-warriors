﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;
using Photon.Pun;

public class destroyAfter : MonoBehaviour
{
    public float timer;
    public GameObject player;
    public bool isFlash, isFrag;
    public CinemachineVirtualCamera cameraObj;
    public int whosThrowable, damage;

    void Start()
    {
        cameraObj = GameObject.Find("CMvcam").GetComponent<CinemachineVirtualCamera>();
        cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
        Destroy(gameObject, timer);

        if (isFlash)
        {
            player = GameObject.FindGameObjectWithTag("Player");

            if (Vector2.Distance(player.transform.position, transform.position) < 30f)
            {
                transform.position = player.transform.position;
            }
            else
            {
                transform.position = new Vector2(3000f, 3000f);
            }
        }

        if (isFrag)
        {
            player = GameObject.FindGameObjectWithTag("Player");

            if (Vector2.Distance(player.transform.position, transform.position) < 30f)
            {
                cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 150;
                Invoke(nameof(makeshake0), 0.08f);
            }
        }
    }

    void makeshake0()
    {
        cameraObj.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isFrag)
        {
            if(collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy")
            {
                collision.GetComponent<playerHealth>().TakeDamage2(damage);

                GameObject[] tag_1 = GameObject.FindGameObjectsWithTag("Player");
                GameObject[] tag_2 = GameObject.FindGameObjectsWithTag("Enemy");
                GameObject[] final_array = tag_1.Concat(tag_2).ToArray();
                for (int i = 0; i < final_array.Length; i++)
                {
                    if (whosThrowable == final_array[i].GetComponent<playerHealth>().whosBullet && whosThrowable != collision.gameObject.GetComponent<playerHealth>().whosBullet)
                    {
                        if (final_array[i].GetComponent<PhotonView>().IsMine)
                        {
                            final_array[i].GetComponent<PhotonView>().RPC("IncreaseXp", RpcTarget.All, damage);
                        }
                    }
                }
            }
        }
    }


}
