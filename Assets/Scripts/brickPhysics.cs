﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyboard.Warriors
{
    public class brickPhysics : MonoBehaviour
    {
        private Rigidbody2D rb;
        private float xMultiplier, yMultiplier;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == 11 || collision.gameObject.layer == 14)
            {
                if(collision.transform.position.x < transform.position.x)
                {
                    xMultiplier = 1;
                }
                else
                {
                    xMultiplier = -1;
                }
                if (collision.transform.position.y < transform.position.y)
                {
                    xMultiplier = 1;
                }
                else
                {
                    yMultiplier = -1;
                }

                rb.velocity = new Vector2(50f * xMultiplier, 50f * yMultiplier);
                
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == 11 || collision.gameObject.layer == 14)
            {
                if (collision.gameObject.GetComponent<bulletMovement>())
                {
                    if (collision.transform.position.x < transform.position.x)
                    {
                        xMultiplier = 1;
                    }
                    else
                    {
                        xMultiplier = -1;
                    }
                    if (collision.transform.position.y < transform.position.y)
                    {
                        xMultiplier = 1;
                    }
                    else
                    {
                        yMultiplier = -1;
                    }

                    
                    rb.velocity = new Vector2
                        (collision.gameObject.GetComponent<bulletMovement>().damage * xMultiplier, collision.gameObject.GetComponent<bulletMovement>().damage * yMultiplier);
                    
                }
            }
        }

    }
}