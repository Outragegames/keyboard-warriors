﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;

public class mollyFire : MonoBehaviour
{
    private ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    public int damage, whosThrowable;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        int i = 0;
        while (i < numCollisionEvents)
        {
            if (other.GetComponent<playerHealth>() != null)
            {
                other.gameObject.GetComponent<playerHealth>().TakeDamage2(damage);

                GameObject[] tag_1 = GameObject.FindGameObjectsWithTag("Player");
                GameObject[] tag_2 = GameObject.FindGameObjectsWithTag("Enemy");
                GameObject[] final_array = tag_1.Concat(tag_2).ToArray();
                if (final_array != null && final_array.Length != 0)
                {
                    for (int j = 0; j < final_array.Length; j++)
                    {
                        if (whosThrowable == final_array[j].GetComponent<playerHealth>().whosBullet && whosThrowable != other.gameObject.GetComponent<playerHealth>().whosBullet)
                        {
                            if (final_array[j].GetComponent<PhotonView>().IsMine)
                            {
                                final_array[j].GetComponent<PhotonView>().RPC("IncreaseXp", RpcTarget.All, damage);
                            }
                        }
                    }
                }
            }
            i++;
        }
    }
}
