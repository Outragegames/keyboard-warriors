﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class playerAnimator : MonoBehaviour
{
    public Animator anim;
    public playerVariables playerVars;
    public PhotonView photonView;

    public string currentState;
    public string playerIdle;
    public string playerWalk;
    public string playerJump;

    void Start()
    {
        playerVars = GetComponent<playerVariables>();
    }

    void Update()
    {
        //if (anim.GetCurrentAnimatorStateInfo(0).IsName(playerJump) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
        //{
        //    playerVars.isJumping = false;
        //}
        if (photonView.IsMine)
        {
            if (playerVars.isWalking)
            {
                if (Input.GetAxisRaw("Horizontal") == 1)
                {
                    if (anim.gameObject.GetComponent<SpriteRenderer>().flipX == true)
                    {
                        //anim.SetFloat("speed", -1f);
                        //ChangeSpeed(-1f);
                        photonView.RPC(nameof(ChangeSpeed), RpcTarget.All, -1f);
                    }
                    else if (anim.gameObject.GetComponent<SpriteRenderer>().flipX == false)
                    {
                        //anim.SetFloat("speed", 1f);
                        //ChangeSpeed(1f);
                        photonView.RPC(nameof(ChangeSpeed), RpcTarget.All, 1f);
                    }
                }
                else if (Input.GetAxisRaw("Horizontal") == -1)
                {
                    if (anim.gameObject.GetComponent<SpriteRenderer>().flipX == true)
                    {
                        //anim.SetFloat("speed", 1f);
                        //ChangeSpeed(1f);
                        photonView.RPC(nameof(ChangeSpeed), RpcTarget.All, 1f);
                    }
                    else if (anim.gameObject.GetComponent<SpriteRenderer>().flipX == false)
                    {
                        //anim.SetFloat("speed", -1f);
                        //ChangeSpeed(-1f);
                        photonView.RPC(nameof(ChangeSpeed), RpcTarget.All, -1f);
                    }
                }
            }

            if (playerVars.isWalking && !playerVars.isJumping)
            {
                if (photonView.IsMine)
                {
                    photonView.RPC(nameof(PlayAnim), RpcTarget.All, playerWalk);
                }
            }
            else if (playerVars.isJumping)
            {
                //PlayAnim(playerJump);
                photonView.RPC(nameof(PlayAnim), RpcTarget.All, playerJump);
            }
            else if (!playerVars.isJumping && !playerVars.isWalking)
            {
                //PlayAnim(playerIdle);
                photonView.RPC(nameof(PlayAnim), RpcTarget.All, playerIdle);
            }
        }
    }

    [PunRPC]
    public void PlayAnim(string newState)
    {
        if (currentState == newState) return;
        anim.Play(newState);
        currentState = newState;
    }

    [PunRPC]
    public void ChangeSpeed(float spd)
    {
        anim.SetFloat("speed", spd);
    }
}
