<Q                         _ADDITIONAL_LIGHTS  	   _EMISSION      _MAIN_LIGHT_SHADOWS    _MAIN_LIGHT_SHADOWS_CASCADE    _SHADOWS_SOFT       a=  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _BaseMap_ST;
	UNITY_UNIFORM vec4 _BaseColor;
	UNITY_UNIFORM vec4 _SpecColor;
	UNITY_UNIFORM vec4 _EmissionColor;
	UNITY_UNIFORM float _Cutoff;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
float u_xlat12;
void main()
{
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _BaseMap_ST.xy + _BaseMap_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 1.17549435e-38);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat1.x = u_xlat0.y * u_xlat0.y;
    u_xlat1.x = u_xlat0.x * u_xlat0.x + (-u_xlat1.x);
    u_xlat2 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat3.x = dot(unity_SHBr, u_xlat2);
    u_xlat3.y = dot(unity_SHBg, u_xlat2);
    u_xlat3.z = dot(unity_SHBb, u_xlat2);
    u_xlat1.xyz = unity_SHC.xyz * u_xlat1.xxx + u_xlat3.xyz;
    u_xlat0.w = 1.0;
    u_xlat2.x = dot(unity_SHAr, u_xlat0);
    u_xlat2.y = dot(unity_SHAg, u_xlat0);
    u_xlat2.z = dot(unity_SHAb, u_xlat0);
    vs_TEXCOORD3.xyz = u_xlat0.xyz;
    u_xlat0.xyz = u_xlat1.xyz + u_xlat2.xyz;
    vs_TEXCOORD1.xyz = max(u_xlat0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    vs_TEXCOORD4.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0_0_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	vec4 _MainLightColor;
uniform 	vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[32];
uniform 	vec4 _AdditionalLightsColor[32];
uniform 	vec4 _AdditionalLightsAttenuation[32];
uniform 	vec4 _AdditionalLightsSpotDir[32];
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
uniform 	vec4 _CascadeShadowSplitSpheres0;
uniform 	vec4 _CascadeShadowSplitSpheres1;
uniform 	vec4 _CascadeShadowSplitSpheres2;
uniform 	vec4 _CascadeShadowSplitSpheres3;
uniform 	vec4 _CascadeShadowSplitSphereRadii;
uniform 	vec4 _MainLightShadowParams;
uniform 	vec4 _MainLightShadowmapSize;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _BaseMap_ST;
	UNITY_UNIFORM vec4 _BaseColor;
	UNITY_UNIFORM vec4 _SpecColor;
	UNITY_UNIFORM vec4 _EmissionColor;
	UNITY_UNIFORM float _Cutoff;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump sampler2D _BaseMap;
UNITY_LOCATION(1) uniform mediump sampler2D _EmissionMap;
UNITY_LOCATION(2) uniform mediump sampler2D _MainLightShadowmapTexture;
UNITY_LOCATION(3) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
layout(location = 0) out highp vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
vec4 u_xlat3;
bvec4 u_xlatb3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
vec4 u_xlat8;
vec4 u_xlat9;
vec4 u_xlat10;
vec4 u_xlat11;
vec2 u_xlat28;
vec2 u_xlat30;
float u_xlat36;
int u_xlati36;
uint u_xlatu36;
float u_xlat37;
uint u_xlatu37;
bool u_xlatb37;
float u_xlat38;
int u_xlati38;
bool u_xlatb38;
float u_xlat39;
uint u_xlatu39;
float u_xlat40;
void main()
{
	ImmCB_0_0_0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	ImmCB_0_0_0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	ImmCB_0_0_0[2] = vec4(0.0, 0.0, 1.0, 0.0);
	ImmCB_0_0_0[3] = vec4(0.0, 0.0, 0.0, 1.0);
    u_xlat0 = texture(_BaseMap, vs_TEXCOORD0.xy);
    u_xlat0.xyz = u_xlat0.xyz * _BaseColor.xyz;
    SV_Target0.w = u_xlat0.w * _BaseColor.w;
    u_xlat1.xyz = texture(_EmissionMap, vs_TEXCOORD0.xy).xyz;
    u_xlat1.xyz = u_xlat1.xyz * _EmissionColor.xyz;
    u_xlat36 = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat2.xyz = vec3(u_xlat36) * vs_TEXCOORD3.xyz;
    u_xlat3.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres0.xyz);
    u_xlat4.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres1.xyz);
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres2.xyz);
    u_xlat6.xyz = vs_TEXCOORD2.xyz + (-_CascadeShadowSplitSpheres3.xyz);
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat3.y = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat3.z = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat3.w = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlatb3 = lessThan(u_xlat3, _CascadeShadowSplitSphereRadii);
    u_xlat4.x = u_xlatb3.x ? float(1.0) : 0.0;
    u_xlat4.y = u_xlatb3.y ? float(1.0) : 0.0;
    u_xlat4.z = u_xlatb3.z ? float(1.0) : 0.0;
    u_xlat4.w = u_xlatb3.w ? float(1.0) : 0.0;
;
    u_xlat3.x = (u_xlatb3.x) ? float(-1.0) : float(-0.0);
    u_xlat3.y = (u_xlatb3.y) ? float(-1.0) : float(-0.0);
    u_xlat3.z = (u_xlatb3.z) ? float(-1.0) : float(-0.0);
    u_xlat3.xyz = u_xlat3.xyz + u_xlat4.yzw;
    u_xlat4.yzw = max(u_xlat3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat36 = dot(u_xlat4, vec4(4.0, 3.0, 2.0, 1.0));
    u_xlat36 = (-u_xlat36) + 4.0;
    u_xlatu36 = uint(u_xlat36);
    u_xlati36 = int(int(u_xlatu36) << 2);
    u_xlat3.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati36 + 1)].xyz;
    u_xlat3.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati36].xyz * vs_TEXCOORD2.xxx + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati36 + 2)].xyz * vs_TEXCOORD2.zzz + u_xlat3.xyz;
    u_xlat3.xyz = u_xlat3.xyz + hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati36 + 3)].xyz;
    u_xlat4.xy = u_xlat3.xy * _MainLightShadowmapSize.zw + vec2(0.5, 0.5);
    u_xlat4.xy = floor(u_xlat4.xy);
    u_xlat3.xy = u_xlat3.xy * _MainLightShadowmapSize.zw + (-u_xlat4.xy);
    u_xlat5 = u_xlat3.xxyy + vec4(0.5, 1.0, 0.5, 1.0);
    u_xlat6 = u_xlat5.xxzz * u_xlat5.xxzz;
    u_xlat28.xy = u_xlat6.yw * vec2(0.0799999982, 0.0799999982);
    u_xlat5.xz = u_xlat6.xz * vec2(0.5, 0.5) + (-u_xlat3.xy);
    u_xlat6.xy = (-u_xlat3.xy) + vec2(1.0, 1.0);
    u_xlat30.xy = min(u_xlat3.xy, vec2(0.0, 0.0));
    u_xlat30.xy = (-u_xlat30.xy) * u_xlat30.xy + u_xlat6.xy;
    u_xlat3.xy = max(u_xlat3.xy, vec2(0.0, 0.0));
    u_xlat3.xy = (-u_xlat3.xy) * u_xlat3.xy + u_xlat5.yw;
    u_xlat30.xy = u_xlat30.xy + vec2(1.0, 1.0);
    u_xlat3.xy = u_xlat3.xy + vec2(1.0, 1.0);
    u_xlat7.xy = u_xlat5.xz * vec2(0.159999996, 0.159999996);
    u_xlat8.xy = u_xlat6.xy * vec2(0.159999996, 0.159999996);
    u_xlat6.xy = u_xlat30.xy * vec2(0.159999996, 0.159999996);
    u_xlat9.xy = u_xlat3.xy * vec2(0.159999996, 0.159999996);
    u_xlat3.xy = u_xlat5.yw * vec2(0.159999996, 0.159999996);
    u_xlat7.z = u_xlat6.x;
    u_xlat7.w = u_xlat3.x;
    u_xlat8.z = u_xlat9.x;
    u_xlat8.w = u_xlat28.x;
    u_xlat5 = u_xlat7.zwxz + u_xlat8.zwxz;
    u_xlat6.z = u_xlat7.y;
    u_xlat6.w = u_xlat3.y;
    u_xlat9.z = u_xlat8.y;
    u_xlat9.w = u_xlat28.y;
    u_xlat3.xyw = u_xlat6.zyw + u_xlat9.zyw;
    u_xlat6.xyz = u_xlat8.xzw / u_xlat5.zwy;
    u_xlat6.xyz = u_xlat6.xyz + vec3(-2.5, -0.5, 1.5);
    u_xlat7.xyz = u_xlat9.zyw / u_xlat3.xyw;
    u_xlat7.xyz = u_xlat7.xyz + vec3(-2.5, -0.5, 1.5);
    u_xlat6.xyz = u_xlat6.yxz * _MainLightShadowmapSize.xxx;
    u_xlat7.xyz = u_xlat7.xyz * _MainLightShadowmapSize.yyy;
    u_xlat6.w = u_xlat7.x;
    u_xlat8 = u_xlat4.xyxy * _MainLightShadowmapSize.xyxy + u_xlat6.ywxw;
    u_xlat28.xy = u_xlat4.xy * _MainLightShadowmapSize.xy + u_xlat6.zw;
    u_xlat7.w = u_xlat6.y;
    u_xlat6.yw = u_xlat7.yz;
    u_xlat9 = u_xlat4.xyxy * _MainLightShadowmapSize.xyxy + u_xlat6.xyzy;
    u_xlat7 = u_xlat4.xyxy * _MainLightShadowmapSize.xyxy + u_xlat7.wywz;
    u_xlat6 = u_xlat4.xyxy * _MainLightShadowmapSize.xyxy + u_xlat6.xwzw;
    u_xlat10 = u_xlat3.xxxy * u_xlat5.zwyz;
    u_xlat11 = u_xlat3.yyww * u_xlat5;
    u_xlat36 = u_xlat3.w * u_xlat5.y;
    vec3 txVec0 = vec3(u_xlat8.xy,u_xlat3.z);
    u_xlat37 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    vec3 txVec1 = vec3(u_xlat8.zw,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec1, 0.0);
    u_xlat38 = u_xlat38 * u_xlat10.y;
    u_xlat37 = u_xlat10.x * u_xlat37 + u_xlat38;
    vec3 txVec2 = vec3(u_xlat28.xy,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec2, 0.0);
    u_xlat37 = u_xlat10.z * u_xlat38 + u_xlat37;
    vec3 txVec3 = vec3(u_xlat7.xy,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec3, 0.0);
    u_xlat37 = u_xlat10.w * u_xlat38 + u_xlat37;
    vec3 txVec4 = vec3(u_xlat9.xy,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec4, 0.0);
    u_xlat37 = u_xlat11.x * u_xlat38 + u_xlat37;
    vec3 txVec5 = vec3(u_xlat9.zw,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec5, 0.0);
    u_xlat37 = u_xlat11.y * u_xlat38 + u_xlat37;
    vec3 txVec6 = vec3(u_xlat7.zw,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec6, 0.0);
    u_xlat37 = u_xlat11.z * u_xlat38 + u_xlat37;
    vec3 txVec7 = vec3(u_xlat6.xy,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec7, 0.0);
    u_xlat37 = u_xlat11.w * u_xlat38 + u_xlat37;
    vec3 txVec8 = vec3(u_xlat6.zw,u_xlat3.z);
    u_xlat38 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec8, 0.0);
    u_xlat36 = u_xlat36 * u_xlat38 + u_xlat37;
    u_xlat37 = (-_MainLightShadowParams.x) + 1.0;
    u_xlat36 = u_xlat36 * _MainLightShadowParams.x + u_xlat37;
#ifdef UNITY_ADRENO_ES3
    u_xlatb37 = !!(0.0>=u_xlat3.z);
#else
    u_xlatb37 = 0.0>=u_xlat3.z;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb38 = !!(u_xlat3.z>=1.0);
#else
    u_xlatb38 = u_xlat3.z>=1.0;
#endif
    u_xlatb37 = u_xlatb37 || u_xlatb38;
    u_xlat36 = (u_xlatb37) ? 1.0 : u_xlat36;
    u_xlat36 = u_xlat36 * unity_LightData.z;
    u_xlat3.xyz = vec3(u_xlat36) * _MainLightColor.xyz;
    u_xlat36 = dot(u_xlat2.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat36 = min(max(u_xlat36, 0.0), 1.0);
#else
    u_xlat36 = clamp(u_xlat36, 0.0, 1.0);
#endif
    u_xlat3.xyz = u_xlat3.xyz * vec3(u_xlat36) + vs_TEXCOORD1.xyz;
    u_xlat36 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu36 = uint(int(u_xlat36));
    u_xlat4.xyz = u_xlat3.xyz;
    for(uint u_xlatu_loop_1 = uint(0u) ; u_xlatu_loop_1<u_xlatu36 ; u_xlatu_loop_1++)
    {
        u_xlati38 = int(uint(u_xlatu_loop_1 & 3u));
        u_xlatu39 = uint(u_xlatu_loop_1 >> 2u);
        u_xlat38 = dot(unity_LightIndices[int(u_xlatu39)], ImmCB_0_0_0[u_xlati38]);
        u_xlati38 = int(u_xlat38);
        u_xlat5.xyz = (-vs_TEXCOORD2.xyz) * _AdditionalLightsPosition[u_xlati38].www + _AdditionalLightsPosition[u_xlati38].xyz;
        u_xlat39 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat39 = max(u_xlat39, 6.10351563e-05);
        u_xlat40 = inversesqrt(u_xlat39);
        u_xlat5.xyz = vec3(u_xlat40) * u_xlat5.xyz;
        u_xlat40 = float(1.0) / float(u_xlat39);
        u_xlat39 = u_xlat39 * _AdditionalLightsAttenuation[u_xlati38].x;
        u_xlat39 = (-u_xlat39) * u_xlat39 + 1.0;
        u_xlat39 = max(u_xlat39, 0.0);
        u_xlat39 = u_xlat39 * u_xlat39;
        u_xlat39 = u_xlat39 * u_xlat40;
        u_xlat40 = dot(_AdditionalLightsSpotDir[u_xlati38].xyz, u_xlat5.xyz);
        u_xlat40 = u_xlat40 * _AdditionalLightsAttenuation[u_xlati38].z + _AdditionalLightsAttenuation[u_xlati38].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat40 = min(max(u_xlat40, 0.0), 1.0);
#else
        u_xlat40 = clamp(u_xlat40, 0.0, 1.0);
#endif
        u_xlat40 = u_xlat40 * u_xlat40;
        u_xlat39 = u_xlat39 * u_xlat40;
        u_xlat6.xyz = vec3(u_xlat39) * _AdditionalLightsColor[u_xlati38].xyz;
        u_xlat38 = dot(u_xlat2.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat38 = min(max(u_xlat38, 0.0), 1.0);
#else
        u_xlat38 = clamp(u_xlat38, 0.0, 1.0);
#endif
        u_xlat4.xyz = u_xlat6.xyz * vec3(u_xlat38) + u_xlat4.xyz;
    }
    SV_Target0.xyz = u_xlat4.xyz * u_xlat0.xyz + u_xlat1.xyz;
    return;
}

#endif
                                $Globals�	        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                     0      _AdditionalLightsColor                    0     _AdditionalLightsAttenuation                  0     _AdditionalLightsSpotDir                  0     _CascadeShadowSplitSpheres0                   p	     _CascadeShadowSplitSpheres1                   �	     _CascadeShadowSplitSpheres2                   �	     _CascadeShadowSplitSpheres3                   �	     _CascadeShadowSplitSphereRadii                    �	     _MainLightShadowParams                    �	     _MainLightShadowmapSize                   �	     _MainLightWorldToShadow                 0         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterialD         _BaseMap_ST                       
   _BaseColor                       
   _SpecColor                           _EmissionColor                    0      _Cutoff                   @          $GlobalsP         _WorldSpaceCameraPos                         unity_MatrixVP                                _BaseMap                  _EmissionMap                _MainLightShadowmapTexture                  UnityPerDraw              UnityPerMaterial          